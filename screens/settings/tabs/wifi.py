import os
from functools import partial
from kivy.lang import Builder
from kivy.properties import StringProperty
from kivymd.demo import AvatarSampleWidget
from kivymd.list import TwoLineAvatarIconListItem
from screens.settings.tabs.base import BaseTab
from kivy.clock import Clock
import utils.net
from widgets.dialog import WiFiInfoDialog, WiFiConnectDialog

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'wifi.kv'))


class WiFiTab(BaseTab):

    cur_ssid = StringProperty('')

    def __init__(self, **kwargs):
        super(WiFiTab, self).__init__(**kwargs)

    def on_enter(self, *args):
        super(WiFiTab, self).on_enter(*args)
        Clock.schedule_once(self.update_ap_list)

    def update_ap_list(self, *args):
        self.ids.ml.clear_widgets()
        ap_list = utils.net.get_ap_list('wlan0')
        cur_ap_name = utils.net.get_current_ap()

        # Move current AP to the beginning of the list
        if cur_ap_name is not None:
            cur_ap = (ap for ap in ap_list if ap['ssid'] == cur_ap_name).next()
            ap_list.remove(cur_ap)
        else:
            cur_ap = None

        ap_list = sorted(ap_list, key=lambda k: k['quality'])
        ap_list.reverse()

        if cur_ap_name is not None:
            ap_list = [cur_ap, ] + ap_list
            self.cur_ssid = cur_ap_name

        for ap in ap_list:
            img_index = ap['quality'] / 20
            if img_index == 5:
                img_index = 4

            txt_ssid = ap['ssid']
            if txt_ssid == cur_ap_name:
                txt_ssid += '   -   Connected'
            try:
                item = TwoLineAvatarIconListItem(type='two-line', text=txt_ssid,
                                                 secondary_text=ap['address'] + (' (encrypted)' if ap['encrypted'] else ''),
                                                 )
                item.add_widget(AvatarSampleWidget(source='images/wifi/{}.png'.format(img_index)))
                item.bind(on_release=partial(self.on_btn, ap['ssid']))
                self.ids.ml.add_widget(item)
            except ValueError:
                pass

    def on_btn(self, ssid, *args):
        if self.cur_ssid == ssid:
            WiFiInfoDialog().open()
        else:
            popup = WiFiConnectDialog(ssid=ssid)
            popup.bind(on_done=self.on_perform_connect)
            popup.open()

    def on_perform_connect(self, *args):
        result_ip = args[1]
        print 'New IP: {}'.format(result_ip)
        Clock.schedule_once(self.update_ap_list)
