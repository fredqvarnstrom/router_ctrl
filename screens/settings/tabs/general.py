import os

from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty
from kivymd.snackbar import Snackbar

from screens.settings.tabs.base import BaseTab
from utils import config_util
from kivy.clock import Clock

from widgets.dialog import AuthDialog

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'general.kv'))


class GeneralTab(BaseTab):

    mode = StringProperty('user')

    def __init__(self, **kwargs):
        super(GeneralTab, self).__init__(**kwargs)

    def on_enter(self, *args):
        Clock.schedule_once(self.update_me)

    def update_me(self, *args):
        self.mode = App.get_running_app().mode
        for mode in ['user', 'admin', 'tech']:
            self.ids['chk_{}'.format(mode)].set_value(self.mode == mode)
        self.root_widget.update_tab_state()

    def btn_apply(self):
        # Get checked value
        cur_mode = 'user'
        for mode in ['user', 'admin', 'tech']:
            if self.ids['chk_{}'.format(mode)].get_value():
                cur_mode = mode
                break
        if cur_mode == 'user':
            App.get_running_app().mode = 'user'
            self.update_me()
        else:
            dlg = AuthDialog(mode=cur_mode)
            dlg.bind(on_success=self.on_success_auth)
            dlg.bind(on_dismiss=self.update_me)
            dlg.open()

    def on_success_auth(self, *args):
        App.get_running_app().mode = args[1]
        self.update_me()
        Snackbar(text='Successfully updated mode').show()
