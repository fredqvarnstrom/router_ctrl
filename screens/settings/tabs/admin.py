import os

from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty
from kivymd.snackbar import Snackbar

import utils.config_util
from screens.settings.tabs.base import BaseTab
from widgets.dialog import NotificationDialog, YesNoDialog

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'admin.kv'))


class AdminTab(BaseTab):

    admin_pwd = StringProperty('')
    tech_pwd = StringProperty('')

    def on_enter(self, *args):
        super(AdminTab, self).on_enter(*args)
        self.init()

    def init(self):
        self.admin_pwd = utils.config_util.get_config('admin', 'password_admin', '')
        self.tech_pwd = utils.config_util.get_config('admin', 'password_tech', '')

    def update_admin_pwd(self):
        self.admin_pwd = self.ids.txt_admin.text
        utils.config_util.set_config('admin', 'password_admin', self.admin_pwd)
        Snackbar(text="Admin password is updated").show()

    def update_tech_pwd(self):
        self.tech_pwd = self.ids.txt_tech.text
        utils.config_util.set_config('admin', 'password_tech', self.tech_pwd)
        Snackbar(text="Technician password is updated").show()

    def on_btn_close(self):
        popup = YesNoDialog(message='Are you sure?', title='Close APP')
        popup.bind(on_confirm=self.close_app)
        popup.open()

    def close_app(self, *args):
        App.get_running_app().stop()
