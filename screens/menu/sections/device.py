import os
import threading
from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty
from kivy.clock import mainthread
from kivymd.snackbar import Snackbar
import utils.common
import utils.config_util
from screens.menu.sections.base import BaseSection
from utils.ssh_util import RouterHacker
from widgets.dialog import LoadingMessageBox

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'device.kv'))


class DeviceSection(BaseSection):

    prev_section = StringProperty('connection_setting_section')
    next_section = StringProperty('')
    dev_name = StringProperty('Trolley')
    dev_num = NumericProperty(1)

    is_tech = False

    def on_enter(self, *args):
        super(DeviceSection, self).on_enter(*args)
        self.prefill_fields()
        self.is_tech = True if App.get_running_app().mode == 'tech' else False
        if not self.is_tech:
            self.ids.tech_container.opacity = 0
            for _key in ['ip', 'username']:
                self.ids[_key].disabled = True

    def prefill_fields(self):
        credential = self.get_credential()
        for _key in ['ip', 'username', 'system_number']:
            if _key in credential:
                self.ids[_key].set_value(credential[_key])

    def set_device(self, dev):
        self.dev_name = dev
        self.title = 'Change the {}'.format(dev)

    def btn_update(self):
        dlg = LoadingMessageBox()
        dlg.title = 'Updating settings of {}...'.format(self.dev_name)
        dlg.size_hint_x = .6
        dlg.open()
        threading.Thread(target=self.update_device, args=(dlg, )).start()

    def update_device(self, *args):
        credential = self.get_credential()
        router_hacker = RouterHacker(host=credential['ip'], port=int(credential['port']), model=credential['model'],
                                     username=credential['username'], password=credential['password'])

        state, reason = router_hacker.connect()
        if not state:
            self.on_finish_update(False, reason, args[0])
        else:
            system_number = self.ids.system_number.get_value()
            if not self.is_tech:
                router_hacker.change_ssid(system_number)
            else:
                _ip = self.ids.ip.get_value()
                _username = self.ids.username.get_value()
                router_hacker.update_params({
                    'ip_address': _ip,
                    'username': _username,
                    'ssid_wired': 'hc{}'.format(system_number),
                    'ssid_wireless': 'hc{}'.format(system_number)
                })
                utils.config_util.set_config(self.get_section_name(), 'ip', _ip)
                utils.config_util.set_config(self.get_section_name(), 'username', _username)
            utils.config_util.set_config(self.get_section_name(), 'system_number', system_number)
            self.on_finish_update(True, None, args[0])

    def get_credential(self):
        credential = utils.config_util.get_section(self.get_section_name())
        return credential

    def get_section_name(self):
        return utils.common.get_device_section_name(self.dev_name)

    @mainthread
    def on_finish_update(self, state, reason, dlg):
        dlg.dismiss()
        if state:
            Snackbar(text='Successfully updated system number of {}.\n'
                          'Please try to connect once it rebooted.'.format(self.dev_name)).show()
        else:
            Snackbar(text='Failed to connect to {}: {}'.format(self.dev_name, reason),
                     background_color=(.8, .3, 0, .5)).show()
