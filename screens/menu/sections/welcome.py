import os
from kivy.lang import Builder
from kivy.properties import StringProperty
from screens.menu.sections.base import BaseSection

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'welcome.kv'))


class WelcomeSection(BaseSection):

    prev_section = StringProperty('welcome_section')
    next_section = StringProperty('connection_setting_section')
    title = StringProperty('Welcome')
