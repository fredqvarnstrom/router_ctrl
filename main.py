# -*- coding: utf-8 -*-
import calendar

from kivy.app import App

import configure
import factory_reg

from kivy.properties import StringProperty, ListProperty
from kivy.uix.screenmanager import SlideTransition
from kivymd.theming import ThemeManager
from screens.menu.screen import MenuScreen
from screens.settings.screen import SettingsScreen
from kivy.clock import Clock
from kivy.uix.screenmanager import ScreenManagerException
from screens.video.screen import VideoScreen
from utils import time_util


class MainApp(App):

    theme_cls = ThemeManager()
    current_title = StringProperty()        # Store title of current screen
    screen_names = ListProperty([])
    screens = {}                            # Dict of all screens
    hierarchy = ListProperty([])

    mode = StringProperty('user')           # `user`, `admin`, `tech`

    def build(self):
        """
        base function of kivy app
        :return:
        """
        self.load_screen()
        self.go_screen('menu', 'right')
        Clock.schedule_interval(self.show_time, 1)

    def show_time(self, *args):
        local_time = time_util.get_local_time()
        str_time = local_time.strftime("%B %d, %Y  %H:%M:%S")
        self.root.ids.lb_time.text = '{}, {}'.format(calendar.day_name[local_time.weekday()][:3], str_time)

    def go_screen(self, dest_screen, direction):
        """
        Go to given screen
        :param dest_screen:     destination screen name
        :param direction:       "up", "down", "right", "left"
        :return:
        """
        if self.current_title != dest_screen:
            try:
                sm = self.root.ids.sm
                sm.transition = SlideTransition()
                screen = self.screens[dest_screen]
                self.root.ids.toolbar.title = screen.title
                sm.switch_to(screen, direction=direction)
                self.current_title = screen.name
            except ScreenManagerException:
                pass

    def load_screen(self):
        """
        Load all screens from data/screens to Screen Manager
        :return:
        """
        self.screen_names = ['menu', 'settings', 'video']
        self.screens = {
            'menu': MenuScreen(),
            'settings': SettingsScreen(),
            'video': VideoScreen(),
        }

    def go_home(self):
        app.go_screen('menu', 'right')
        self.screens['menu'].switch_section('welcome_section', 'up')

if __name__ == '__main__':

    app = MainApp()
    app.run()
