"""
    sudo apt-get install python-pip iceweasel xvfb
    sudo pip install pyvirtualdisplay selenium
    sudo apt-get install python-kivy (for 3.5" touchscreen)

    Class for acting with Selenium package
    refer : http://selenium-python.readthedocs.io/

"""
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
import time


class RouterCtrl():

    def __init__(self, url, name, pwd):
        self.base_url = url     # "https://67.187.1.144:8080/"
        self.username = name    # "root"
        self.password = pwd     # "Heimdall"
        self.driver = webdriver.Firefox()

    def login(self, url):
        """
        before moving to this url, login
        :param url:
        :return:
        """
        print self.base_url, url

        self.driver.get(self.base_url + url)    # since user is not logged in, login page will be displayed

        usr_elem = self.driver.find_element_by_name("username")
        usr_elem.clear()
        usr_elem.send_keys(self.username)

        pwd_elem = self.driver.find_element_by_name("password")
        pwd_elem.clear()
        pwd_elem.send_keys(self.password)

        btn_elem = self.driver.find_element_by_xpath("//input[@type='submit']")
        btn_elem.send_keys(Keys.RETURN)

        if self.driver.title == 'Login':    # if page is still on login page
            print "Failed to login, invalid username/password"
            return False
        else:
            print "Logged in..."
            return True

    def change_ssid(self, new_id):
        """
        change the SSID of router
        :param new_id: new id of router
        :return:
        """
        if not self.login('/link.cgi'):
            return False

        ssid_elem = self.driver.find_element_by_name("essid")

        if ssid_elem.get_attribute('value') == new_id:
            print "Already set"
            return False

        ssid_elem.clear()
        ssid_elem.send_keys(new_id)

        btn_elem = self.driver.find_element_by_xpath("//input[@type='submit'][@value='Change']")
        btn_elem.send_keys(Keys.RETURN)
        return self.apply_changes()

    def apply_changes(self):
        """
        Press 'Apply' button after changing some configurations
        :return: If no changes are detected, 'Apply' button does not appear and return false
        """
        try:
            confirm_btn_elem = self.driver.find_element_by_id("apply_button")
            confirm_btn_elem.send_keys(Keys.RETURN)
            self.close()
            return True
        except NoSuchElementException:
            print "No changes are detected..."
            self.close()
            return False

    def upload_config(self, filename):
        """
        upload new configuration file
        :param filename:
        :return:
        """
        if not self.login('/system.cgi'):
            return False

        # set configuration file
        f_elem = self.driver.find_element_by_id('cfgfile')
        f_elem.send_keys(filename)

        # press 'Upload' button
        btn_elem = self.driver.find_element_by_id('cfgupload')
        btn_elem.send_keys(Keys.RETURN)

        return self.apply_changes()

    def get_cur_ssid(self):
        if not self.login('/link.cgi'):
            return None
        ssid_elem = self.driver.find_element_by_name("essid")
        ssid = ssid_elem.get_attribute('value')
        self.close()
        return ssid

    def close(self):
        self.driver.close()
        # display.stop()


if __name__ == "__main__":

    start_time = time.time()

    # r_ctrl = RouterCtrl("https://67.187.1.144:8080/", "root", "Heimdall")
    r_ctrl = RouterCtrl("https://192.168.0.254", "root", "Heimdall")

    new_ssid = "hc444"
    r_ctrl.change_ssid(new_ssid)
#    r_ctrl.upload_config('/home/pi/router_ctrl/test.cfg')

#    r_ctrl.close()

    print "Elapsed time : ", time.time() - start_time



